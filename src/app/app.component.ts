import { Component } from '@angular/core';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

import { Router, ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Información',
      url: '/informacion',
      icon: 'information-circle-outline'
    },
    {
      title: 'Programa',
      url: '/programa',
      icon: 'list-box'
    },
    {
      title: 'Preguntas',
      url: '/home',
      icon: 'help'
    },
    {
      title: 'Relatores',
      url: '/ponentes',
      icon: 'contact'
    },
    {
      title: 'Usuarios',
      url: '/usuarios',
      icon: 'contacts'
    }
  ];
  user;
  imagen: string;
  nombre: string;
  apellidos: string;
  color: string;
  UID;

  private snapshotChangesSubscription: any;

  constructor(private platform: Platform,
              private splashScreen: SplashScreen,
              private statusBar: StatusBar,
              private router: Router,
              public afAuth: AngularFireAuth,
              public afs: AngularFirestore,
              public menuCtrl: MenuController,
              public route: ActivatedRoute) {
            this.initializeApp();
            this.recuperarUsuario();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.afAuth.user.subscribe(user => {
        if (user) {
          this.router.navigate(['/home']);
          this.splashScreen.hide();
        } else {
          this.router.navigate(['/informacion']);
          this.splashScreen.hide();
        }
        this.user = user.uid;
      }, err => {
        this.router.navigate(['/login']);
        this.splashScreen.hide();
      });
      this.statusBar.styleDefault();
    });
  }


  recuperarUsuario() {
    this.afAuth.user.subscribe(currentUser => {
      return new Promise<any>((resolve, reject) => {
        console.log(currentUser.uid);
        // let currentUser = firebase.auth().currentUser;
        this.snapshotChangesSubscription = this.afs.doc<any>('usuarios/' + currentUser.uid).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
          console.log(snapshots.image);
          this.imagen = snapshots.image;
          this.nombre = snapshots.nombre;
          this.apellidos  = snapshots.apellidos;
          this.color  = snapshots.color;
          this.UID = currentUser.uid;
        }, err => {
          reject(err);
        });
      });

    });
  }

  async viewDetails(id) {
    this.route.snapshot.paramMap.getAll(id);
    this.router.navigate(['/details/' + id]);
    this.menuCtrl.close();
  }

/*
  viewDetails(id, item) {
    this.route.snapshot.paramMap.getAll(item);
    this.router.navigateByUrl('/details/' + id);
    this.menuCtrl.close();
  }
*/

}
