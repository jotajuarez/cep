import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';




@Injectable()
export class FcmServices {
  private snapshotChangesSubscription: any;

  files;
  images;

  constructor(public afs: AngularFirestore,
              private platform: Platform) {

    }
getInformacion() {
        return new Promise<any>((resolve, reject) => {
              this.snapshotChangesSubscription = this.afs.collection('informacion').snapshotChanges();
              resolve(this.snapshotChangesSubscription);
        });
      }

getPrograma() {
        return new Promise<any>((resolve, reject) => {
              this.snapshotChangesSubscription = this.afs.collection('programa', ref => ref.orderBy('orden', 'asc')).snapshotChanges();
              resolve(this.snapshotChangesSubscription);
        });
      }

}