import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import * as firebase from 'firebase/app';
import 'firebase/storage';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';

export interface Usuario {
  id?: string;
  name: string;
  apellidos: string;
  email: string;
  image: string;
  ponente: boolean;
  moderador: boolean;
  permisos: string;
}

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  private usersCollection: AngularFirestoreCollection<Usuario>;

  private users: Observable<Usuario[]>;

  image: any;


  constructor(public db: AngularFirestore) {

    this.usersCollection = db.collection<Usuario>('userProfile');

    this.users = this.usersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  createUsuarios(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.db.collection('usuarios').doc(currentUser.uid).set({
        nombre: value.name,
        apellidos: value.apellidos,
        email: value.email,
        image: value.image,
        ponente: false,
        moderador: false,
        color: value.color,
        permisos: 'usuario',
        cargo: 'Usuario Común',
        orden: 100
      })
      .then(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

  getUsers() {
    return this.users;
  }

  getUser(id) {
    return this.usersCollection.doc<Usuario>(id).valueChanges();
  }

  updateUser(users: Usuario, id: string) {
    return this.usersCollection.doc(id).update(users);
  }

  addUser(users: Usuario) {
    return this.usersCollection.add(users);
  }

  removeUser(id) {
    return this.usersCollection.doc(id).delete();
  }

}