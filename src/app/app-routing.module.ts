import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './services/auth.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: './pages/login/login.module#LoginPageModule',
  },
  {
    path: 'home',
    loadChildren: './pages/home/home.module#HomePageModule',
    canActivate: [AuthService]
  },
  {
    path: 'list',
    loadChildren: './pages/list/list.module#ListPageModule',
    canActivate: [AuthService]
  },
  { path: 'ponentes',
    loadChildren: './pages/ponentes/ponentes.module#PonentesPageModule',
    canActivate: [AuthService]
  },
  {
    path: 'usuarios',
    loadChildren: './pages/usuarios/usuarios.module#UsuariosPageModule',
    canActivate: [AuthService]
  },
  { path: 'register',
  loadChildren: './pages/register/register.module#RegisterPageModule'
  },
  {
    path: 'preguntamodal',
    loadChildren: './pages/preguntamodal/preguntamodal.module#PreguntamodalPageModule'
  },
  {
    path: 'details/:id',
    loadChildren: './pages/details/details.module#DetailsPageModule'
  },
  {
    path: 'newponente/:id',
    loadChildren: './pages/newponente/newponente.module#NewponentePageModule'
   },
  {
    path: 'respondermodal',
    loadChildren: './pages/respondermodal/respondermodal.module#RespondermodalPageModule'
  },
  { path: 'pantalla',
    loadChildren: './pages/pantalla/pantalla.module#PantallaPageModule'
  },
  { path: 'informacion',
    loadChildren: './pages/informacion/informacion.module#InformacionPageModule'
  },
  { path: 'programa',
  loadChildren: './pages/programa/programa.module#ProgramaPageModule'
  },
    { path: 'recordar',
    loadChildren: './pages/recordar/recordar.module#RecordarPageModule' 
  }




];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})

export class AppRoutingModule {}
