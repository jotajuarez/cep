import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FcmServices } from '../../services/fmc.service';

@Component({
  selector: 'app-informacion',
  templateUrl: './informacion.page.html',
  styleUrls: ['./informacion.page.scss'],
})
export class InformacionPage implements OnInit {

  items: Array<any>;

   public snapshotChangesSubscription: any;

   usuario: string;
   color: string;


  constructor(public fcmServices: FcmServices,
              public authService: AuthService,
              public route: ActivatedRoute,
              public router: Router,
              public afAuth: AngularFireAuth,
              public afs: AngularFirestore) { }

  ngOnInit() {
    this.getInfo();
    this.recuperarUsuario();
  }

  getInfo() {
    this.fcmServices.getInformacion()
    .then(data => {
      data.subscribe(informacion => {
        this.items = informacion;
        console.log(this.items);
      });
    });
  }

  logout() {
    this.authService.doLogout()
    .then(res => {
      this.router.navigate(['/login']);
    }, err => {
      // debugger;
      console.log(err);
    });
  }

  recuperarUsuario() {
    this.afAuth.user.subscribe(currentUser => {
      this.usuario = currentUser.uid;
     // console.log(this.usuario);
      return new Promise<any>((resolve, reject) => {
        // let currentUser = firebase.auth().currentUser;
        this.snapshotChangesSubscription = this.afs.doc<any>('usuarios/' + this.usuario).valueChanges()
        .subscribe(snapshots => {
         resolve(snapshots);
          this.color = snapshots.color;
        }, err => {
          reject(err);
        });
      });

    });
  }
}
