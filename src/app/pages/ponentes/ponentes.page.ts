import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';

import { AuthService } from '../../services/auth.service';
import { PreguntasService } from '../../services/preguntas.service';
import { UsuariosService } from '../../services/usuarios.service';

import { ModalController } from '@ionic/angular';

import {  PreguntamodalPage } from '../preguntamodal/preguntamodal.page';

import { DetailsPage } from '../details/details.page';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-ponentes',
  templateUrl: './ponentes.page.html',
  styleUrls: ['./ponentes.page.scss'],
})
export class PonentesPage implements OnInit {

  items: Array<any>;
  item: any;

  private snapshotChangesSubscription: any;

  usuario: string;
  usuarioLog;
  color: string;

  constructor(private authService: AuthService,
              public usuariosServices: UsuariosService,
              public afAuth: AngularFireAuth,
              public route: ActivatedRoute,
              public router: Router,
              public modalCtrl: ModalController,
              public preguntasServices: PreguntasService,
              public storage: Storage,
              public afs: AngularFirestore
              ) {


      }


      ngOnInit() {
        this.getData();
        this.recuperarUsuario();
      }

      recuperarUsuario() {
        this.afAuth.user.subscribe(currentUser => {
          this.usuario = currentUser.uid;
          console.log(this.usuario);

          return new Promise<any>((resolve, reject) => {
            this.snapshotChangesSubscription = this.afs.doc<any>('usuarios/' + currentUser.uid).valueChanges()
            .subscribe(snapshots => {
              resolve(snapshots);
              this.usuarioLog = snapshots.permisos;
              this.color = snapshots.color;
              console.log(this.usuarioLog);
              console.log(this.color);
            }, err => {
              reject(err);
            });
          });

        });
      }


      getData() {
        this.preguntasServices.getTasks()
        .then(data => {
          data.subscribe(usuarios => {
            this.items = usuarios;
            console.log(this.items);
          });
        });
      }

      logout() {
        this.authService.doLogout()
        .then(res => {
          this.router.navigate(['/login']);
        }, err => {
          // debugger;
          console.log(err);
        });
      }


}


