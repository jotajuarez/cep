import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';


// import md5 from 'crypto-md5';
// import {Md5} from 'md5-typescript';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  validations_form: FormGroup;
  errorMessage = '';

  validation_messages = {
   'email': [
     { type: 'required', message: 'O email e requerido.' },
     { type: 'pattern', message: 'Engadir un email válido.' }
   ],
   'password': [
     { type: 'required', message: 'O contrasinal e requerido.' },
     { type: 'minlength', message: 'o contrasinal debe ter ao menos 5 caracteres.' }
   ]
 };

 // profilePicture: any = 'assets/imgs/default_image.jpg';
 // email: any;


  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {

    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
  }
  tryLogin(value) {
    this.authService.doLogin(value)
    .then(res => {
      this.router.navigate(['/informacion']);

    }, err => {
      this.errorMessage = err.message;
      console.log(err);
    });
  }

  goRegisterPage() {
    this.router.navigate(['/register']);
  }

}
