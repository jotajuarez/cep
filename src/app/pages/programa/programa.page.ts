import { Component, OnInit } from '@angular/core';
import { FcmServices } from '../../services/fmc.service';

import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';

import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-programa',
  templateUrl: './programa.page.html',
  styleUrls: ['./programa.page.scss'],
})
export class ProgramaPage implements OnInit {

  items: Array<any>;
  public snapshotChangesSubscription: any;

  usuario: string;
  color: string;

  constructor(public fcmServices: FcmServices,
              public route: ActivatedRoute,
              public router: Router,
              public authService: AuthService,
              public afAuth: AngularFireAuth,
              public afs: AngularFirestore) { }

  ngOnInit() {
    this.getInfo();
    this.recuperarUsuario();
  }

  getInfo() {
    this.fcmServices.getPrograma()
    .then(data => {
      data.subscribe(programa => {
        this.items = programa;
        console.log(this.items);
      });
    });
  }

  logout() {
    this.authService.doLogout()
    .then(res => {
      this.router.navigate(['/login']);
    }, err => {
      // debugger;
      console.log(err);
    });
  }

  recuperarUsuario() {
    this.afAuth.user.subscribe(currentUser => {
      this.usuario = currentUser.uid;
      return new Promise<any>((resolve, reject) => {
        this.snapshotChangesSubscription = this.afs.doc<any>('usuarios/' + this.usuario).valueChanges()
        .subscribe(snapshots => {
         resolve(snapshots);
          this.color = snapshots.color;
        }, err => {
          reject(err);
        });
      });

    });
  }

}
