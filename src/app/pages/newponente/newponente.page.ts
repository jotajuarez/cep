import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { PreguntasService } from './../../services/preguntas.service';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-newponente',
  templateUrl: './newponente.page.html',
  styleUrls: ['./newponente.page.scss'],
})
export class NewponentePage implements OnInit {

  validations_form: FormGroup;
  image: any;
  item: any;
  load = false;

  color: string;
  email: string;

  items;
  id;


  public routeData;

  selectedCheckbox: any;
  esponente = false;
  esmoderador = false;

  public snapshotChangesSubscription: any;

  usuarioLog;

  constructor(public toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              private formBuilder: FormBuilder,
              private preguntasService: PreguntasService,
              private alertCtrl: AlertController,
              private route: ActivatedRoute,
              private router: Router,
              public afAuth: AngularFireAuth,
              public afs: AngularFirestore) {

              this.selectedCheckbox = {};

             }

  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      ponente: new FormControl(this.esponente, Validators.compose([
        Validators.required
      ])),
      moderador: new FormControl(this.esmoderador, Validators.compose([
        Validators.required
      ])),
    });



    this.getData();

  }

  getChanged(e) {
    if (!this.selectedCheckbox[e]) {

      this.selectedCheckbox[e] = true;
    } else {
      this.selectedCheckbox[e] = false;
    }
    console.log(this.selectedCheckbox);
    this.esponente = e.detail.checked;

    console.log(this.esponente);
  }

  getChangedModerador(m) {
    if (!this.selectedCheckbox[m]) {

      this.selectedCheckbox[m] = true;
    } else {
      this.selectedCheckbox[m] = false;
    }
    console.log(this.selectedCheckbox);
    this.esmoderador = m.detail.checked;

    console.log(this.esmoderador);
  }

  getData() {
    const todoId = this.route.snapshot.paramMap.get('id');
    this.afAuth.user.subscribe(currentUser => {
      return new Promise<any>((resolve, reject) => {
        this.snapshotChangesSubscription = this.afs.doc<any>('usuarios/' + todoId).valueChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
          this.usuarioLog = snapshots;
          console.log(this.usuarioLog);
        }, err => {
          reject(err);
        });
      });

    });
  }


  onSubmit(value) {
    const todoId = this.route.snapshot.paramMap.get('id');

      let data = {
        nombre: this.usuarioLog.nombre,
        apellidos: this.usuarioLog.apellidos,
        email: this.usuarioLog.email,
        moderador: this.esmoderador,
        ponente: this.esponente,
        permisos: 'usuario',
        color: this.usuarioLog.color,
        image: this.usuarioLog.image
      };
      this.preguntasService.updateTask(todoId, data)
      .then(
        res => {
          this.dismiss();
        }
      );

  }

  dismiss() {
    this.router.navigate(['/home']);
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm',
      message: 'Do you want to delete ' + this.item.title + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {}
        },
        {
          text: 'Yes',
          handler: () => {
            this.preguntasService.deleteTask(this.item.id)
            .then(
              res => {
               // debugger
                this.dismiss();
              },
              err => console.log(err)
            );
          }
        }
      ]
    });
    await alert.present();
  }


  async presentLoading(loading) {
    return await loading.present();
  }


}

