import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewponentePage } from './newponente.page';

describe('NewponentePage', () => {
  let component: NewponentePage;
  let fixture: ComponentFixture<NewponentePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewponentePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewponentePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
