import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

import { AuthService } from '../../services/auth.service';
import { PreguntasService } from '../../services/preguntas.service';
import { UsuariosService } from '../../services/usuarios.service';

import { ModalController, ToastController, Platform } from '@ionic/angular';

import { PreguntamodalPage } from '../preguntamodal/preguntamodal.page';
import { RespondermodalPage } from '../respondermodal/respondermodal.page';
import { PantallaPage } from './../pantalla/pantalla.page';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})


export class HomePage implements OnInit {

  items: Array<any>;
  preguntas: Array<any>;
  pregunta: any;
  respuestas: Array<any>;
  respuesta: any;
  item: any;

 // public PreguntaIdDb: any;
  public snapshotChangesSubscription: any;

  usuario: string;
  usuarioLog;
  esponente: boolean;
  esmoderador: boolean;
  message;
  animacion: boolean;
  showImage;
  color: string;
  contador: number;
  UID;


  constructor(private authService: AuthService,
              public usuariosServices: UsuariosService,
              public afAuth: AngularFireAuth,
              public route: ActivatedRoute,
              public router: Router,
              public modalCtrl: ModalController,
              public preguntasServices: PreguntasService,
              public afs: AngularFirestore,
              public platform: Platform,
              public toastCtrl: ToastController) {

      }


      ngOnInit() {
        this.getData();
        this.getPreguntas();
        this.getRespuestas();
        this.recuperarUsuario();
      }

      async presentToast() {
        const toast = await this.toastCtrl.create({
          message: this.message,
          duration: 3000
        });
        toast.present();
      }

     recuperarUsuario() {
        this.afAuth.user.subscribe(currentUser => {
          this.usuario = currentUser.uid;
         // console.log(this.usuario);
          return new Promise<any>((resolve, reject) => {
            // let currentUser = firebase.auth().currentUser;
            this.snapshotChangesSubscription = this.afs.doc<any>('usuarios/' + this.usuario).valueChanges()
            .subscribe(snapshots => {
             resolve(snapshots);
              this.usuarioLog = snapshots.permisos;
              this.esponente = snapshots.ponente;
              this.esmoderador = snapshots.moderador;
              this.color = snapshots.color;
              this.UID = currentUser.uid;
            }, err => {
              reject(err);
            });
          });

        });
      }


      getData() {
        this.preguntasServices.getTasks()
        .then(data => {
          data.subscribe(usuarios => {
            this.items = usuarios;
          });
        });
      }

      async openNewUserModal() {
        let modal = await this.modalCtrl.create({
          component: PreguntamodalPage
        });
        modal.present();
      }

      logout() {
        this.authService.doLogout()
        .then(res => {
          this.router.navigate(['/login']);
        }, err => {
          // debugger;
          console.log(err);
        });
      }

getPreguntas() {
  this.preguntasServices.getPreguntas()
  .then(data => {
    data.subscribe(preguntas => {
      this.preguntas = preguntas;
      console.log(this.preguntas.length);
      this.showImage = true;

      setTimeout(() => {
            this.showImage = false;
       }, 6000);
    });
  });
}



// detectar cambios y crear animación //
 cerrarNotificacion() {
  this.preguntasServices.animacion = false;
}

getRespuestas() {
  this.preguntasServices.getRespuestas()
  .then(data => {
    data.subscribe(respuestas => {
      this.respuestas = respuestas;
     // console.log(this.respuestas);
    });
  });
}

async responderPreguntaModal(id, data) {
  console.log(id);
  let modal = await this.modalCtrl.create({
    component: RespondermodalPage,
    id: id
  });
  modal.present();
}

async pantallaCompleta(id) {

  this.preguntasServices.getPreguntaPantalla(id);

  let modal = await this.modalCtrl.create({
    component: PantallaPage,
    id: id
  });
  modal.present();
}

}


