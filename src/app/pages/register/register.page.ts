import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { UsuariosService } from '../../services/usuarios.service';
import { Router } from '@angular/router';
import { LoadingController, ToastController, ModalController } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';

import { PreguntasService } from '../../services/preguntas.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  validations_form: FormGroup;
  errorMessage = '';
  successMessage = '';

  image: any;

  validation_messages = {
    'name': [
      { type: 'required', message: 'O nome e requerido.' },
      { type: 'pattern', message: 'Engadir un nome válido.' }
    ],
    'apellidos': [
      { type: 'required', message: 'Os apelidos son requeridos.' },
      { type: 'pattern', message: 'Engadir unos apelidos válidos.' }
    ],
   'email': [
     { type: 'required', message: 'O email e requerido' },
     { type: 'pattern', message: 'Engadir un email válido.' }
   ],
   'password': [
     { type: 'required', message: 'O password e requerido.' },
     { type: 'minlength', message: 'O contrasinal debe ter ao menos 5 caracteres.' }
   ],
   'color': [
     { type: 'required', message: 'Elixa una cor para seu usuario.' },
     { type: 'minlength', message: 'Seleccione una cor.' }
   ]
  };

  colores =[
    { nombre: 'Verde', hex: '#0cd1e8' },
    { nombre: 'Azul',  hex: '#3880ff' },
    { nombre: 'Violeta', hex: '#7044ff' },
    { nombre: 'Amarelo', hex: '#ffce00' },
    { nombre: 'Roxo', hex: '#f04141'},
    { nombre: 'Negro', hex: '#000000'}
     ];

  constructor(private authService: AuthService,
              private formBuilder: FormBuilder,
              private router: Router,
              private usuariosServices: UsuariosService,
              private imagePicker: ImagePicker,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              private modalCtrl: ModalController,
              private webview: WebView,
              private preguntasServices: PreguntasService) {

              }

  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      name: new FormControl('', Validators.compose([
        Validators.minLength(3),
        Validators.required
      ])),
      apellidos: new FormControl('', Validators.compose([
        Validators.minLength(3),
        Validators.required
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
      color: new FormControl('', Validators.compose([
        Validators.required
      ])),
    });

    this.resetFields();

  }

  resetFields() {
    this.image = 'assets/imgs/default_image.jpg';
  }

  dismiss() {
   this.modalCtrl.dismiss();
  }

  tryRegister(value) {
    console.log(value.color);
    this.authService.doRegister(value)
     .then(res => {
      // console.log(res);
        const data = value;
        const credentials = {
          name: data.name,
          apellidos: data.apellidos,
          email: value.email,
          color: value.color,
          image: this.image,
          password: value.password,
        };
      this.usuariosServices.createUsuarios(credentials);
       // this.errorMessage = '';
      // this.successMessage = 'Your account has been created. Please log in.';
      this.router.navigate(['/informacion']);
      }, err => {
       console.log(err);
       this.errorMessage = err.message;
       this.successMessage = '';
     });
  }

  goLoginPage() {
    this.router.navigate(['/login']);
  }

/*
  openImagePicker() {
    this.imagePicker.hasReadPermission()
    .then((result) => {
      if (result === false) {
        // no callbacks required as this opens a popup which returns async
        this.imagePicker.requestReadPermission();
      } else if (result === true) {
        this.imagePicker.getPictures({
          maximumImagesCount: 1
        }).then(
          (results) => {
            for (let i = 0; i < results.length; i++) {
              this.uploadImageToFirebase(results[i]);
            }
          }, (err) => console.log(err)
        );
      }
    }, (err) => {
      console.log(err);
    });
  }

  async uploadImageToFirebase(image) {
    const loading = await this.loadingCtrl.create({
      message: 'Por favor espere...'
    });
    const toast = await this.toastCtrl.create({
      message: 'La imagen fue subida correctamente',
      duration: 3000
    });
    this.presentLoading(loading);
    let image_src = this.webview.convertFileSrc(image);
    let randomId = Math.random().toString(36).substr(2, 5);

    // uploads img to firebase storage
    this.preguntasServices.uploadImage(image_src, randomId)
    .then(photoURL => {
      this.image = photoURL;
      loading.dismiss();
      toast.present();
    }, err => {
      console.log(err);
    });
  }

  async presentLoading(loading) {
    return await loading.present();
  }
*/
}

